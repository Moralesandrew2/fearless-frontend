function createCard(name, description, pictureUrl, starts, ends) {
    return `
    <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts}-${ends}</div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!(response.ok)) {

        }
        else{
            const data = await response.json();


        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts);
                const endDate = new Date(details.conference.ends);
                const starts = startDate.toLocaleDateString();
                const ends = endDate.toLocaleDateString();
                const html = createCard(name, description, pictureUrl, starts, ends)
                const column = document.querySelector('.col');
                column.innerHTML += html;
            }
        }

        }

        } catch (e) {

        }
});
